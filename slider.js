let left = document.querySelector(".left");
let right = document.querySelector(".right");
let slider = document.querySelector(".slider");

let bottom = document.querySelector(".bottom");
let slideNumber = 1;
let image = document.querySelectorAll(".image");
let length = image.length;

for (let i = 0; i < length; i++) {
  const div = document.createElement("div");
  div.className =
    "navdot w-5 cursor-pointer h-5 rounded-full border border-4 border-white";

  bottom.appendChild(div);
}

const navdots = document.querySelectorAll(".navdot");
navdots[0].style.backgroundColor = "yellow";

const removeBg = () => {
  navdots.forEach((navdot) => {
    navdot.style.backgroundColor = "transparent";
  });
};

navdots.forEach((navdot, i) => {
  navdot.addEventListener("click", () => {
    removeBg();
    slider.style.transform = `translateX(-${i * 800}px)`;
    slideNumber = i + 1;
    navdot.style.backgroundColor = "yellow";
  });
});

const bgChange = () => {
  removeBg();
  navdots[slideNumber - 1].style.backgroundColor = "yellow";
};
const getNextImage = () => {
  slider.style.transform = `translateX(-${slideNumber * 800}px)`;
  slideNumber++;
};
const getFirstImage = () => {
  slider.style.transform = `translateX(0px)`;
  slideNumber = 1;
};
const getLastImage = () => {
  slider.style.transform = `translateX(-${(length - 1) * 800}px)`;
  slideNumber = length;
};
const getPreviousImage = () => {
  slider.style.transform = `translateX(-${(slideNumber - 2) * 800}px)`;
  slideNumber--;
};
right.addEventListener("click", () => {
  slideNumber < length ? getNextImage() : getFirstImage();
  bgChange();
});

left.addEventListener("click", () => {
  slideNumber > 1 ? getPreviousImage() : getLastImage();
  bgChange();
});
